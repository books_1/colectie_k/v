# V

## Content

```
./V. E. Schwab:
V. E. Schwab - Culorile Magiei - V1 Partea intunecata a magiei 1.0 '{Supranatural}.docx
V. E. Schwab - Culorile Magiei - V2 Adunarea umbrelor 1.0 '{Supranatural}.docx

./V. Firsov:
V. Firsov - Ingerii 0.99 '{SF}.docx

./V. I. Ciuikov:
V. I. Ciuikov - Sfarsitul Reichului hitlerist 1.0 '{Istorie}.docx

./V. Komarov:
V. Komarov - Revolutia se amana 0.99 '{SF}.docx
V. Komarov - Solutia 0.99 '{SF}.docx

./V. Lascu:
V. Lascu - Tehnica speologiei alpine 0.99 '{Manual}.docx

./V. Pavica & M. Iordan:
V. Pavica & M. Iordan - Din tainele razboiului sub apa 0.9 '{MistersiStiinta}.docx

./V. S. Naipaul:
V. S. Naipaul - Mascaricii 0.9 '{Literatura}.docx

./V. Tevekelian:
V. Tevekelian - Agentia de publicitate a domnului Kocek 1.0 '{Politista}.docx

./Vaclan Erben:
Vaclan Erben - Femeia disparuta 1.0 '{Politista}.docx

./Vaclav Havel:
Vaclav Havel - Despre identitatea umana 1.0 '{Diverse}.docx

./Vaclav Nyvlt:
Vaclav Nyvlt - Doua milioane de martori 1.0 '{Politista}.docx

./Val Antim:
Val Antim - Fenomen inexplicabil de violare 0.7 '{Diverse}.docx
Val Antim - Un vis pentru doi 1.0 '{SF}.docx

./Val Buzoianu:
Val Buzoianu - In pat cu Carol al II-lea 1.0 '{Literatura}.docx

./Valentin Borda & Nicolae Simion:
Valentin Borda & Nicolae Simion - Ani de drumetie 0.9 '{Natura}.docx

./Valentin Corban:
Valentin Corban - Vulturul Mortii - V1 0.8 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V2 0.8 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V3 2.0 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V4 2.0 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V5 2.0 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V6 2.0 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V7 2.0 '{Aventura}.docx
Valentin Corban - Vulturul Mortii - V8 2.0 '{Aventura}.docx

./Valentin Gustav:
Valentin Gustav - Capitanul Fetelor Negre 1.0 '{Aventura}.docx
Valentin Gustav - Insula robilor 1.0 '{Aventura}.docx

./Valentin Neacsu:
Valentin Neacsu - Bigamul 0.9 '{Diverse}.docx

./Valentin Radulescu:
Valentin Radulescu - Iscoada mintii 1.0 '{Matematica}.docx

./Valentin Rasputin:
Valentin Rasputin - Despartirea de Matiora 1.0 '{Literatura}.docx
Valentin Rasputin - Sorocul cel de pe urma 0.6 '{Literatura}.docx
Valentin Rasputin - Traieste si ia aminte 1.0 '{Literatura}.docx

./Valerie Duhamel:
Valerie Duhamel - Singuratatea Alexandrei 0.9 '{Dragoste}.docx
Valerie Duhamel - Valea opalelor 0.9 '{Dragoste}.docx

./Valerie Parv:
Valerie Parv - Sotul disparut 1.0 '{Romance}.docx

./Valerie Zayne:
Valerie Zayne - Zorii sperantei 0.99 '{Dragoste}.docx

./Valerio Evangelisti:
Valerio Evangelisti - Taina inchizitorului Eymerich 0.99 '{AventuraIstorica}.docx

./Valerio Massimo Manfredi:
Valerio Massimo Manfredi - Akropolis 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Alexandru cel Mare - V1 Fiul visului 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Alexandru cel Mare - V2 Nisipurile lui Amon 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Alexandru cel Mare - V3 Capatul lumii 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Faraonul nisipurilor 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Idele lui Martie 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Imparatia dragonilor 1.1 '{AventuraIstorica}.docx
Valerio Massimo Manfredi - Ultima legiune 1.1 '{AventuraIstorica}.docx

./Valeriu Bargau & Neculai Chirica:
Valeriu Bargau & Neculai Chirica - Utopia profesorului Dunca 1.0 '{SF}.docx

./Valeriu Butulescu:
Valeriu Butulescu - Insula femeilor 0.9 '{Teatru}.docx

./Valeriu Ciuculin:
Valeriu Ciuculin - Cainele prin apa V1 0.99 '{Literatura}.docx
Valeriu Ciuculin - Cainele prin apa V2 0.99 '{Literatura}.docx
Valeriu Ciuculin - Cainele prin apa V3 0.99 '{Literatura}.docx
Valeriu Ciuculin - Cainele prin apa V4 0.99 '{Literatura}.docx
Valeriu Ciuculin - Limanul 0.9 '{Literatura}.docx

./Valeriu Dobrescu:
Valeriu Dobrescu - Istorioare moral religioase 2.0 '{Spiritualitate}.docx

./Valeriu Gafencu:
Valeriu Gafencu - Indreptar la spovedanie 0.9 '{Religie}.docx

./Valeriu Popa:
Valeriu Popa - Marturii 0.9 '{Sanatate}.docx
Valeriu Popa - Regimuri, cauze spirituale ale bolilor 1.0 '{Sanatate}.docx
Valeriu Popa - Regimuri 0.8 '{Sanatate}.docx

./Valeri Uvarov:
Valeri Uvarov - Piramidele 0.9 '{MistersiStiinta}.docx

./Val McDermid:
Val McDermid - Dincolo de sange 0.99 '{Thriller}.docx

./Val Mcdermid:
Val Mcdermid - Febra oaselor 1.0 '{Thriller}.docx

./Vandana Singh:
Vandana Singh - Delhi 0.8 '{Diverse}.docx

./Vanga Dimitrova:
Vanga Dimitrova - Despre Romania 0.9 '{Spiritualitate}.docx
Vanga Dimitrova - Leacuri de vindecare 0.8 '{Spiritualitate}.docx

./Vanora Bennet:
Vanora Bennet - Regina matasurilor 2.0 '{AventuraIstorica}.docx
Vanora Bennet - Sange regesc 2.0 '{AventuraIstorica}.docx

./Vasile Alecsandri:
Vasile Alecsandri - Cantece batranesti 1.0 '{Versuri}.docx
Vasile Alecsandri - Chirita 1.0 '{Teatru}.docx
Vasile Alecsandri - Despot voda 1.0 '{Teatru}.docx
Vasile Alecsandri - Doine 1.0 '{Versuri}.docx
Vasile Alecsandri - Hore 1.0 '{Versuri}.docx
Vasile Alecsandri - Istoria unui galben 1.0 '{Teatru}.docx
Vasile Alecsandri - Pasteluri 1.0 '{Versuri}.docx
Vasile Alecsandri - Peatra din casa 1.0 '{Teatru}.docx
Vasile Alecsandri - Rusaliile 0.9 '{Teatru}.docx
Vasile Alecsandri - Sanziana si Pepelea 1.0 '{Teatru}.docx
Vasile Alecsandri - Suvenire din Italia. Buchetiera de la Florenta 1.0 '{ClasicRo}.docx
Vasile Alecsandri - Suvenire din viata mea 1.0 '{ClasicRo}.docx
Vasile Alecsandri - Vasile Porojan 1.0 '{ClasicRo}.docx

./Vasile Cojocaru:
Vasile Cojocaru - Corida cu melci 2.0 '{Politista}.docx

./Vasile Danion:
Vasile Danion - Despre cunoasterea lui Dumnezeu 0.9 '{Religie}.docx
Vasile Danion - Despre reincarnare si invazia extraterestra 0.99 '{Religie}.docx
Vasile Danion - Drumul spre acasa 1.0 '{Religie}.docx
Vasile Danion - Evadarea din inchisoarea ingerilor cazuti 1.0 '{Religie}.docx
Vasile Danion - Intre iubire si pacat 0.99 '{Religie}.docx
Vasile Danion - Jurnalul convertirii 0.8 '{Religie}.docx
Vasile Danion - Mangaiere pentru bolnavi 0.99 '{Religie}.docx
Vasile Danion - Ne vorbesc parinti athoniti 0.99 '{Religie}.docx
Vasile Danion - Pacate noi, pacate vechi 0.99 '{Religie}.docx
Vasile Danion - Patericul mirenilor 0.9 '{Religie}.docx
Vasile Danion - Povesti pentru copiii mari si mici 0.99 '{Religie}.docx
Vasile Danion - Repere. Duhovnicul. Rugaciunea. Postul 0.9 '{Religie}.docx
Vasile Danion - Scrieri catre un prieten 0.9 '{Religie}.docx

./Vasile Dumitru Fulger:
Vasile Dumitru Fulger - Sectia amazoanelor 1.1 '{Spionaj}.docx

./Vasile Manuceanu:
Vasile Manuceanu - Gorganul 1.0 '{IstoricaRo}.docx
Vasile Manuceanu - Logofatul Dragu 1.0 '{IstoricaRo}.docx

./Vasile Voiculescu:
Vasile Voiculescu - Ciobanila 0.9 '{ProzaScurta}.docx
Vasile Voiculescu - Ciorba de bolovan 1.0 '{Diverse}.docx
Vasile Voiculescu - Din tara Zimbrului 1.0 '{Versuri}.docx
Vasile Voiculescu - Fata din Java 1.0 '{Diverse}.docx
Vasile Voiculescu - Perna de puf 0.9 '{Diverse}.docx
Vasile Voiculescu - Pescarul Amin 0.9 '{Diverse}.docx
Vasile Voiculescu - Poezii 1.0 '{Versuri}.docx
Vasile Voiculescu - Povestiri V1 1.0 '{IstoricaRo}.docx
Vasile Voiculescu - Povestiri V2 1.0 '{IstoricaRo}.docx
Vasile Voiculescu - Sakuntala 0.9 '{IstoricaRo}.docx
Vasile Voiculescu - Spiritul rascumparator de moarte. Tatal nostru 0.9 '{Diverse}.docx
Vasile Voiculescu - Ultimele sonete inchipuite ale lui Shakespeare 1.0 '{Versuri}.docx
Vasile Voiculescu - Varsta de fier 1.0 '{Versuri}.docx
Vasile Voiculescu - Viscolul 0.9 '{Razboi}.docx

./Vasili Grossman:
Vasili Grossman - Viata si Destin 1.1 '{Interzisa}.docx

./Vasili Suksin:
Vasili Suksin - Calina rosie 1.0 '{Literatura}.docx
Vasili Suksin - Liubavinii 1.1 '{Literatura}.docx

./Vassilis Alexakis:
Vassilis Alexakis - Dupa Isus Hristos 0.8 '{Diverse}.docx

./Veit Etzold:
Veit Etzold - Clara Vidalis - V2 Agonie mentala 1.0 '{Supranatural}.docx
Veit Etzold - Taietura finala 1.0 '{Politista}.docx

./Venedikt Erofeev:
Venedikt Erofeev - Moscova Petuski 1.0 '{Literatura}.docx

./Veniamin Kaverin:
Veniamin Kaverin - In fata oglinzii 1.0 '{Literatura}.docx

./Vernor Vinge:
Vernor Vinge - Adancurile cerului 3.1 '{SF}.docx
Vernor Vinge - Foc in adanc 2.1 '{SF}.docx
Vernor Vinge - La capatul curcubeului 1.0 '{SF}.docx

./Veronica Alina Constanceanu:
Veronica Alina Constanceanu - O poveste verde 0.99 '{Diverse}.docx

./Veronica D. Niculescu:
Veronica D. Niculescu - Rosu, rosu catifea 1.0 '{Diverse}.docx

./Veronica Gherasim:
Veronica Gherasim - Caseta Colombinei 1.0 '{Teatru}.docx

./Veronica Micle:
Veronica Micle - Drag mi-ai fost 1.0 '{Versuri}.docx

./Veronica Roth:
Veronica Roth - Divergent - V1 Devergent 1.0 '{SF}.docx
Veronica Roth - Divergent - V2 Insurgent 1.0 '{SF}.docx
Veronica Roth - Divergent - V3 Experiment 1.0 '{SF}.docx
Veronica Roth - Divergent - V4 Four 1.0 '{SF}.docx

./Veronica Stevens:
Veronica Stevens - Femeia in alb 0.99 '{Romance}.docx

./Vesna Goldsworthy:
Vesna Goldsworthy - Marele Gorsky 1.0 '{Literatura}.docx

./Vianna Stibal:
Vianna Stibal - Theta Healing 0.8 '{Sanatate}.docx

./Vicente Blasco Ibanez:
Vicente Blasco Ibanez - Casa blestemata 1.0 '{Literatura}.docx
Vicente Blasco Ibanez - Cei patru cavaleri ai apocalipsului 1.0 '{AventuraIstorica}.docx
Vicente Blasco Ibanez - Maja Desnuda 0.7 '{Literatura}.docx
Vicente Blasco Ibanez - Mare Nostrum 1.0 '{Literatura}.docx
Vicente Blasco Ibanez - Papa marii 1.0 '{AventuraIstorica}.docx
Vicente Blasco Ibanez - Printre portocali 1.0 '{Dragoste}.docx
Vicente Blasco Ibanez - Rafael si Maria 1.0 '{Dragoste}.docx

./Vicente Ibanez:
Vicente Ibanez - Seducatoarea 0.7 '{Dragoste}.docx

./Victor Barladeanu:
Victor Barladeanu - Exilatul din Planetopolis 2.0 '{SF}.docx
Victor Barladeanu - Gheata de foc 2.0 '{SF}.docx
Victor Barladeanu - Operatiunea Psycho 1.0 '{SF}.docx

./Victor Bercescu:
Victor Bercescu - Bombardament cu asteroizi 1.0 '{Politista}.docx
Victor Bercescu - Un milion de lire 2.0 '{Politista}.docx

./Victor Cilinca:
Victor Cilinca - O scrisoare gasita 0.9 '{Teatru}.docx

./Victor Cojocaru:
Victor Cojocaru - Celsius 41,1 0.6 '{Diverse}.docx
Victor Cojocaru - Psihiatrie sociala 0.8 '{Diverse}.docx

./Victor Del Arbol:
Victor Del Arbol - Tristetea samuraiului 1.0 '{Literatura}.docx

./Victor Eftimiu:
Victor Eftimiu - Kimonoul instelat 2.0 '{Diverse}.docx
Victor Eftimiu - Pe urmele zimbrului 1.0 '{IstoricaRo}.docx

./Victor Erofeev:
Victor Erofeev - Cinci fluvii ale vietii 0.9 '{Necenzurat}.docx
Victor Erofeev - Frumoasa rusoaica 1.0 '{Literatura}.docx

./Victor Frankl:
Victor Frankl - Omul in cautarea sensului vietii 1.0 '{Filozofie}.docx

./Victor Hugo:
Victor Hugo - Anul 93 1.0 '{ClasicSt}.docx
Victor Hugo - Bug Jargal 2.0 '{ClasicSt}.docx
Victor Hugo - Cea din urma zi 1.0 '{ClasicSt}.docx
Victor Hugo - Han din Islanda 3.0 '{ClasicSt}.docx
Victor Hugo - Mizerabilii - V1 Fantine 2.0 '{ClasicSt}.docx
Victor Hugo - Mizerabilii - V2 Cosette 2.0 '{ClasicSt}.docx
Victor Hugo - Mizerabilii - V3 Marius 2.0 '{ClasicSt}.docx
Victor Hugo - Mizerabilii - V4 Idila 2.0 '{ClasicSt}.docx
Victor Hugo - Mizerabilii - V5 Jean Valjean 2.0 '{ClasicSt}.docx
Victor Hugo - Notre Dame de Paris 1.0 '{ClasicSt}.docx
Victor Hugo - Oamenii marii 1.0 '{ClasicSt}.docx
Victor Hugo - Omul care rade 2.0 '{ClasicSt}.docx
Victor Hugo - Rinul. Anexe desene 1.0 '{ClasicSt}.docx
Victor Hugo - Rinul 2.0 '{ClasicSt}.docx
Victor Hugo - Scrisori din calatorie 2.0 '{ClasicSt}.docx
Victor Hugo - Ultima zi a unui condamnat la moarte 1.0 '{ClasicSt}.docx

./Victoria Alexander:
Victoria Alexander - Effington Family - V1 Dragoste de proba 1.0 '{Romance}.docx
Victoria Alexander - Effington Family - V2 Cum sa alegi sotul potrivit 1.0 '{Romance}.docx
Victoria Alexander - Effington Family - V3 Lectia de dragoste 1.0 '{Romance}.docx
Victoria Alexander - Effington Family - V4 Mireasa pentru un print 1.0 '{Romance}.docx
Victoria Alexander - Effington Family - V5 Inaltimea sa, sotia mea 1.0 '{Romance}.docx
Victoria Alexander - Effington Family - V6 Indragostita de sotul potrivit 1.0 '{Romance}.docx
Victoria Alexander - Effington Family - V7 Marturisirea 1.0 '{Romance}.docx
Victoria Alexander - Millworth Manor - V1 Sarada 1.0 '{Romance}.docx
Victoria Alexander - Millworth Manor - V2 Avantajul de a fi pacatos 1.0 '{Romance}.docx
Victoria Alexander - Millworth Manor - V3 Aventuri compromitatoare 1.0 '{Romance}.docx

./Victoria Aveyard:
Victoria Aveyard - Red Queen - V1 Regina rosie 1.0 '{SF}.docx
Victoria Aveyard - Red Queen - V2 Sabia de sticla 1.0 '{SF}.docx
Victoria Aveyard - Red Queen - V3 Colivia regelui 1.0 '{SF}.docx
Victoria Aveyard - Red Queen - V4 Furtuna razboiului 1.0 '{SF}.docx

./Victoria Grant:
Victoria Grant - Caile destinului 0.9 '{Dragoste}.docx

./Victoria Leigh:
Victoria Leigh - Dragoste de sora 0.99 '{Dragoste}.docx

./Victoria Serman:
Victoria Serman - Ramai insula mea de canabis 1.0 '{Literatura}.docx

./Victor Kernbach:
Victor Kernbach - Enigmele miturilor astrale 1.0 '{MistersiStiinta}.docx
Victor Kernbach - Luntrea sublima 1.0 '{SF}.docx
Victor Kernbach - Miturile esentiale 1.0 '{MistersiStiinta}.docx
Victor Kernbach - Povestiri ciudate 1.0 '{SF}.docx
Victor Kernbach - Umbra timpului 1.0 '{SF}.docx
Victor Kernbach - Vacantele secrete 1.1 '{CalatorieinTimp}.docx

./Victor Loghin:
Victor Loghin - Copacul Bodhi 1.0 '{Spiritualitate}.docx

./Victor M. Bucur:
Victor M. Bucur - Razboi total 0.99 '{Razboi}.docx

./Victor Martin:
Victor Martin - Carte de citit la volan. Aforisme 0.99 '{Aforisme}.docx
Victor Martin - Disparitia 0.9 '{SF}.docx
Victor Martin - Fantana 0.99 '{SF}.docx
Victor Martin - La pescuit 5.0 '{SF}.docx
Victor Martin - Lumea zabrelelor de puscarie 0.99 '{SF}.docx
Victor Martin - Masina de scris carti 0.99 '{SF}.docx
Victor Martin - Omul providential 0.99 '{SF}.docx
Victor Martin - Partidul de export 0.99 '{SF}.docx
Victor Martin - Viata e un cantec 0.99 '{SF}.docx

./Victor Negulescu:
Victor Negulescu - Spionaj si contraspionaj 0.99 '{Istorie}.docx

./Victor Ostrovsky:
Victor Ostrovsky - Leul din Iudeea 1.0 '{Suspans}.docx

./Victor Potra:
Victor Potra - 3 Pasi in rai - Primul pas 0.9 '{SF}.docx

./Victor Rusu Ciobanu:
Victor Rusu Ciobanu - Dacia Felix - V1 Dacia Felix 1.0 '{IstoricaRo}.docx
Victor Rusu Ciobanu - Dacia Felix - V2 Fiul adoptiv 1.0 '{IstoricaRo}.docx
Victor Rusu Ciobanu - Dacia Felix - V3 Un destin aparte 1.0 '{IstoricaRo}.docx

./Victor Suvorov:
Victor Suvorov - Acvarium 2.0 '{ActiuneRazboi}.docx
Victor Suvorov - Epurarea 1.0 '{Razboi}.docx
Victor Suvorov - Imi retrag cuvintele 1.0 '{Razboi}.docx
Victor Suvorov - Sinuciderea 1.0 '{Razboi}.docx
Victor Suvorov - Spargatorul de gheata 1.0 '{Razboi}.docx
Victor Suvorov - Spetnaz 1.0 '{Razboi}.docx
Victor Suvorov - Ultima republica 1.0 '{ActiuneRazboi}.docx
Victor Suvorov - Ultima Republica V1 1.0 '{Razboi}.docx
Victor Suvorov - Ultima Republica V2 1.0 '{Razboi}.docx
Victor Suvorov - Ultima Republica V3 1.0 '{Razboi}.docx
Victor Suvorov - Umbra victoriei 1.0 '{Razboi}.docx
Victor Suvorov - Ziua M 1.0 '{ActiuneRazboi}.docx

./Viet Thanh Nguyen:
Viet Thanh Nguyen - Simpatizantul 1.0 '{Literatura}.docx

./Vikas Swarup:
Vikas Swarup - Vagabondul milionar 2.0 '{Literatura}.docx

./Viktor Astafiev:
Viktor Astafiev - Un pastor si o pastorita 1.0 '{Dragoste}.docx
Viktor Astafiev - Visul crestelor albe 1.0 '{Literatura}.docx

./Villiers de L'isle Adam:
Villiers de L'isle Adam - Vera 0.99 '{Diverse}.docx

./Vince Flynn:
Vince Flynn - Mitch Rapp - V1 Asasin american 1.0 '{Suspans}.docx
Vince Flynn - Mitch Rapp - V2 Lovitura fatala 1.0 '{Suspans}.docx
Vince Flynn - Mitch Rapp - V3 Transfer de putere 1.5 '{Suspans}.docx

./Vincent Gabarra:
Vincent Gabarra - Amurgul barbatilor 1.0 '{ActiuneComando}.docx

./Vintila Corbul:
Vintila Corbul - Caderea Constantinopolelui V1 2.0 '{Aventura}.docx
Vintila Corbul - Caderea Constantinopolelui V2 2.0 '{Aventura}.docx
Vintila Corbul - Cavalcada in iad V1 1.0 '{Aventura}.docx
Vintila Corbul - Cavalcada in iad V2 1.0 '{Aventura}.docx
Vintila Corbul - Cenusa si orhidee la New York 1.0 '{Aventura}.docx
Vintila Corbul - Dinastia Sunderland Beauclair - V1 Idolii de aur V1 3.0 '{Aventura}.docx
Vintila Corbul - Dinastia Sunderland Beauclair - V1 Idolii de aur V2 3.0 '{Aventura}.docx
Vintila Corbul - Dinastia Sunderland Beauclair - V1 Idolii de aur V3 2.0 '{Aventura}.docx
Vintila Corbul - Dinastia Sunderland Beauclair - V2 Pasari de prada V1 3.0 '{Aventura}.docx
Vintila Corbul - Dinastia Sunderland Beauclair - V2 Pasari de prada V2 3.0 '{Aventura}.docx
Vintila Corbul - Dinastia Sunderland Beauclair - V2 Pasari de prada V3 3.0 '{Aventura}.docx
Vintila Corbul - Extaz, moarte si rock'n roll 1.0 '{Aventura}.docx
Vintila Corbul - Imparateasa fara coroana 1.0 '{Aventura}.docx
Vintila Corbul - Iubirile imposibile ale lui Petronius 1.0 '{Aventura}.docx
Vintila Corbul - Salvati-ma sunt miliardar 1.0 '{Aventura}.docx

./Vintila Corbul & Eugen Burada:
Vintila Corbul & Eugen Burada - Cenusa si orhidee. Moarte si portocale. In jet 2.0 '{Aventura}.docx
Vintila Corbul & Eugen Burada - Groaza vine de pretutindeni 2.1 '{Aventura}.docx
Vintila Corbul & Eugen Burada - Moarte si portocale la Palermo 2.0 '{Aventura}.docx
Vintila Corbul & Eugen Burada - Uragan deasupra Europei 2.0 '{Aventura}.docx

./Vintila Corbul & Mircea Burada:
Vintila Corbul & Mircea Burada - Roxelana si Soliman 1.0 '{Aventura}.docx

./Vintila Horia:
Vintila Horia - Cavalerul resemnarii 1.0 '{AventuraIstorica}.docx
Vintila Horia - Dumnezeu s-a nascut in exil 1.0 '{Biografie}.docx
Vintila Horia - Mai sus de miazanoapte 1.0 '{AventuraIstorica}.docx
Vintila Horia - Memoriile unui fost Sagetator 1.0 '{Literatura}.docx
Vintila Horia - O femeie pentru apocalips 1.0 '{AventuraIstorica}.docx

./Viola Larsen:
Viola Larsen - Un barbat romantic 0.9 '{Dragoste}.docx

./Violet Winspear:
Violet Winspear - Valea Raintree 1.0 '{Romance}.docx

./Viorel Burlacu:
Viorel Burlacu - Rocada tragica 1.0 '{ClubulTemerarilor}.docx

./Viorel Cacoveanu:
Viorel Cacoveanu - Cu moartea intre patru ochi 1.0 '{Politista}.docx
Viorel Cacoveanu - Mortii nu mint niciodata 1.0 '{Politista}.docx

./Viorel Darie:
Viorel Darie - Adoris si Kromia 1.0 '{AventuraIstorica}.docx

./Viorel Gheorghita:
Viorel Gheorghita - ET EGO. Sarata - Pitesti - Gherla - Aiud 0.9 '{Biografie}.docx

./Viorel Marineasa:
Viorel Marineasa - Hexamer 0.9 '{Diverse}.docx

./Viorel Pahomi:
Viorel Pahomi - Ora 5 0.99 '{Diverse}.docx

./Viorel Parligras:
Viorel Parligras - Curcubeu pe cer 0.99 '{SF}.docx

./Viorel Rosu:
Viorel Rosu - Dezvaluiri cutremuratoare despre francmasonerie 0.8 '{Politica}.docx

./Viorel Zaicu:
Viorel Zaicu - Declaratie 0.9 '{Diverse}.docx
Viorel Zaicu - Inserare 0.9 '{Diverse}.docx

./Viorica Huber:
Viorica Huber - Taina sfinxului de pe marte 2.0 '{SF}.docx

./Viorica Marica Hagianu:
Viorica Marica Hagianu - Vocea fetei de piatra 0.9 '{SF}.docx

./Virgil Duda:
Virgil Duda - Cora 1.0 '{Dragoste}.docx

./Virgil Ierunca:
Virgil Ierunca - Fenomenul Pitesti 1.0 '{Istorie}.docx

./Virgil Madgearu:
Virgil Madgearu - Agrarianism, capitalism, imperialism 1.0 '{Politica}.docx

./Virgil Stoenescu:
Virgil Stoenescu - O fata imposibila 1.0 '{Teatru}.docx

./Virginia Woolf:
Virginia Woolf - Doamna Dalloway 0.8 '{Literatura}.docx

./Virginio Marafante:
Virginio Marafante - Capcana kryanilor 1.0 '{SF}.docx

./Vitali Gubarev:
Vitali Gubarev - Imparatia oglinzilor strambe 1.0 '{BasmesiPovesti}.docx

./Vitali Gubariov:
Vitali Gubariov - Calatorie pe steaua diminetii 1.0 '{SF}.docx

./Vitalii Trenev:
Vitalii Trenev - Indienii 1.0 '{Diverse}.docx

./Vivian Stuart:
Vivian Stuart - Iarna in Canare 0.9 '{Dragoste}.docx
Vivian Stuart - Mireasa lui Darnley 0.99 '{Dragoste}.docx

./Vladimir Afanasievici Obrucev:
Vladimir Afanasievici Obrucev - Plutonia 1.0 '{SF}.docx
Vladimir Afanasievici Obrucev - Tara lui Sannikov 1.0 '{SF}.docx

./Vladimir Besleaga:
Vladimir Besleaga - Ignat si Ana 0.99 '{Diverse}.docx

./Vladimir Braghin:
Vladimir Braghin - In tara codrilor de iarba 2.0 '{SF}.docx

./Vladimir Colin:
Vladimir Colin - A zecea lume 1.0 '{SF}.docx
Vladimir Colin - Babel 1.0 '{SF}.docx
Vladimir Colin - Basme 1.0 '{BasmesiPovesti}.docx
Vladimir Colin - Capcanele timpului 2.0 '{CalatorieinTimp}.docx
Vladimir Colin - Dincolo de zidul de neon 1.0 '{SF}.docx
Vladimir Colin - Dintii lui Cronos - V1 Dintii lui Cronos 1.0 '{SF}.docx
Vladimir Colin - Dintii lui Cronos - V2 Broasca 0.99 '{SF}.docx
Vladimir Colin - Grifonul lui Ulise 1.0 '{SF}.docx
Vladimir Colin - Imposibila oaza 1.0 '{SF}.docx
Vladimir Colin - Intoarcerea pescarusului 1.0 '{SF}.docx
Vladimir Colin - Legendele tarii lui Vam 1.0 '{SF}.docx
Vladimir Colin - Pentagrama 1.0 '{SF}.docx
Vladimir Colin - Timp cu calaret si corb 1.0 '{SF}.docx
Vladimir Colin - Torpilorul rosu 1.0 '{Teatru}.docx
Vladimir Colin - Viitorul al doilea 1.0 '{SF}.docx

./Vladimir Hanga:
Vladimir Hanga - Alexandru cel Mare 1.0 '{IstoricaRo}.docx

./Vladimir Henzl:
Vladimir Henzl - Aventurile doctorului Haig 1.0 '{Aventura}.docx

./Vladimir Medvedev:
Vladimir Medvedev - Zahhak 1.0 '{Literatura}.docx

./Vladimir Megre:
Vladimir Megre - Cedrii Sunatori ai Rusiei - V1 Anastasia 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V2 Cedrii sunatori ai Rusiei 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V3 Spatiul de iubire 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V4 Creatia 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V5 Cine suntem noi 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V6 Cartea neamului 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V7 Energia vietii 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V8.1 Noua civilizatie 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V8.2 Ritualurile iubirii 1.0 '{Spiritualitate}.docx
Vladimir Megre - Cedrii Sunatori ai Rusiei - V10 - Anasta 1.0 '{Spiritualitate}.docx

./Vladimir Nabokov:
Vladimir Nabokov - Adevarata viata a lui Sebastian Knight 1.0 '{Literatura}.docx
Vladimir Nabokov - Apararea Lujin 1.0 '{Literatura}.docx
Vladimir Nabokov - Blazon de bastard 1.0 '{Literatura}.docx
Vladimir Nabokov - Darul 1.0 '{Literatura}.docx
Vladimir Nabokov - Disperare 1.0 '{Literatura}.docx
Vladimir Nabokov - Glorie 1.0 '{Literatura}.docx
Vladimir Nabokov - Invitatie la esafod 1.0 '{Literatura}.docx
Vladimir Nabokov - Lolita 1.0 '{Literatura}.docx
Vladimir Nabokov - Lucruri transparente 1.0 '{Literatura}.docx
Vladimir Nabokov - Masenka 1.0 '{Literatura}.docx
Vladimir Nabokov - Ochiul 1.0 '{Literatura}.docx
Vladimir Nabokov - Pnin 1.0 '{Literatura}.docx
Vladimir Nabokov - Priveste-i pe Arlechini 1.0 '{Literatura}.docx
Vladimir Nabokov - Rege, dama, valet 1.0 '{Literatura}.docx
Vladimir Nabokov - Un hohot in bezna 1.0 '{Literatura}.docx
Vladimir Nabokov - Vrajitorul 1.0 '{Literatura}.docx

./Vladimir Pustan:
Vladimir Pustan - Tradata iubirea trecea 0.7 '{Diverse}.docx

./Vladimir Savcenko:
Vladimir Savcenko - Uluitoarea descoperire a lui V.V. Krivosein 1.0 '{SF}.docx

./Vladimir Sorokin:
Vladimir Sorokin - Gheata 1.0 '{SF}.docx
Vladimir Sorokin - Ziua opricinicului 1.0 '{SF}.docx

./Vladimir Voinovici:
Vladimir Voinovici - Nemaipomenitele ispravi ale soldatului Cionchin 1.0 '{Tineret}.docx

./Vladimir Volkoff:
Vladimir Volkoff - Complotul 0.7 '{Suspans}.docx
Vladimir Volkoff - Rapirea 1.0 '{Suspans}.docx
Vladimir Volkoff - Strutocamila 0.99 '{Suspans}.docx
Vladimir Volkoff - Tratat de dezinformare 0.99 '{Istorie}.docx
Vladimir Volkoff - Treimea raului 0.9 '{Comunism}.docx

./Vladimir Znosko:
Vladimir Znosko - Viata sf. Teofil, nebunul pentru Hristos 0.9 '{Biografie}.docx

./Vlad Musatescu:
Vlad Musatescu - Aventuri aproximative V1 1.0 '{Aventura}.docx
Vlad Musatescu - Aventuri aproximative V2 1.0 '{Aventura}.docx
Vlad Musatescu - Aventuri aproximative V3 1.0 '{Aventura}.docx
Vlad Musatescu - Cei trei veseli naparstoci 2.0 '{Aventura}.docx
Vlad Musatescu - Contratimp V1 2.0 '{Aventura}.docx
Vlad Musatescu - Contratimp V2 2.0 '{Aventura}.docx
Vlad Musatescu - De-a baba oarba 1.0 '{Aventura}.docx
Vlad Musatescu - De-a v-ati ascunselea 2.0 '{Aventura}.docx
Vlad Musatescu - Expeditia Nisetrul 2 2.0 '{Aventura}.docx
Vlad Musatescu - Extravagantul Conan 2 V1 2.0 '{Aventura}.docx
Vlad Musatescu - Extravagantul Conan 2 V2 2.0 '{Aventura}.docx
Vlad Musatescu - La sud de lacul Nairobi 0.9 '{Aventura}.docx
Vlad Musatescu - Oameni de buna credinta 1.0 '{Aventura}.docx
Vlad Musatescu - Unchiul Andi, detectivul si nepotii sai 1.0 '{Aventura}.docx

./Vlad T. Popescu:
Vlad T. Popescu - Vrajitoarele grase nu sunt arse pe rug 0.99 '{Literatura}.docx

./Voicu Bugariu:
Voicu Bugariu - Animalul de beton 0.9 '{SF}.docx
Voicu Bugariu - Literatii se amuzau 2.0 '{Politista}.docx
Voicu Bugariu - Literati si sefisti 0.99 '{SF}.docx
Voicu Bugariu - Lumea lui Als Ob 1.0 '{SF}.docx
Voicu Bugariu - Sfera 2.0 '{SF}.docx
Voicu Bugariu - Visul lui Stephen King 0.9 '{SF}.docx
Voicu Bugariu - Vocile vikingilor 2.0 '{SF}.docx

./Voltaire:
Voltaire - Candid sau optimismul 2.0 '{Filozofie}.docx
Voltaire - Naivul 2.0 '{Filozofie}.docx

./Vonda N. Mcintyre:
Vonda N. Mcintyre - Despre ceata, iarba si nisip 0.99 '{SF}.docx

./Vuk Draskovic:
Vuk Draskovic - Consulul rus 0.99 '{Diverse}.docx
```

